"""

Copyright (C) 2019 Gabriel Araújo <gabriel140492@gmail.com>

Philosopher Dinner is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the
License, or (at your option) any later version.

Philosopher Dinner is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public
License along with this program; if not, see <http://www.gnu.org/licenses/>.
"""


from threading import Thread, Lock, Condition
from random import randint
from time import sleep

class Fork():
    def __init__(self):
        self.lock = Lock()
        self.available = True
        self.used_by = None

class Spaghetti:
    def __init__(self):
        self.amount = 10

    def decrease_amount(self):
        if self.amount == 0: return False
        self.amount = self.amount - 1
        return True

class Philosopher():
    def __init__(self, name, left_fork, right_fork):
        self.name = name
        self.forks = [left_fork, right_fork]
        self.fork_on_hand = [False for i in range(0, len(self.forks))]
        self.state = None

    def eat(self, spaghetti):
        self.get_forks()
        for _ in range(randint(1, 10)):
            if not self.update_status("  🍝  ", spaghetti):
                break
            sleep(1)
        self.give_back_forks()

    def think(self):
        # update global state
        self.update_status("  💭  ")
        sleep(randint(1, 10))

    def get_forks(self):
        self.update_status("  ⌛  ")
        already_on_hand = self.fork_on_hand[:]
        while(not all(on_hand for on_hand in already_on_hand)):
            self.give_back_forks(already_on_hand)
            self.try_get_forks()
            already_on_hand = self.fork_on_hand[:]

    def try_get_forks(self):
        for i in range(0, len(self.forks)):
            self.try_get_fork(i)

    def try_get_fork(self, index):
        # If the fork is already on hand we need not to try get it again
        if self.fork_on_hand[index]:
            return True

        # Try get the fork in a random timeout
        fork = self.forks[index]
        if not fork.lock.acquire(timeout=randint(1, 10) * 0.001):
            return False
        self.fork_on_hand[index] = True

        # update global state
        # the fork in now NOT available
        self.update_status(fork=(fork, False))
        return True

    def give_back_forks(self, forks=None):
        if not forks:
            forks = self.fork_on_hand
        for i in range(0, len(self.forks)):
            if forks[i]:
                self.give_back_fork(i)

    def give_back_fork(self, index):
        # update global state
        # the fork in now available
        fork = self.forks[index]
        self.fork_on_hand[index] = False
        self.update_status(fork=(fork, True))
        fork.lock.release()

    def update_status(self, state = None, spaghetti = None, fork = None):
        display_cv.acquire()

        if fork:
            (fork, available) = fork
            fork.available = available
            fork.used_by = None if available else self.name
        
        changed = False
        if spaghetti:
            changed = spaghetti.decrease_amount()

        if state:
            changed = changed or self.state != state
            self.state = state

        if changed: display_cv.notify()
        display_cv.release()
        return changed

def main():
    threads = [ Thread(target=philosopher_thread, args=args, name=args[0].name) for args in zip(philosophers, spaghettis) ]
    for thread in threads: thread.start()
    display_cv.acquire()
    while not all(p.state == "  💤  " for p in philosophers):
        print_status()
        # display_cv.release()
        # sleep(.5)
        # display_cv.acquire()
        display_cv.wait()
    print_status()
    display_cv.release()

    for thread in threads: thread.join()

def philosopher_thread(philosopher, spaghetti):
    while(spaghetti.amount > 0):
        philosopher.eat(spaghetti)
        philosopher.think()
    philosopher.update_status("  💤  ") 

def print_status():
    print('Philosophers state')
    print('==================')
    print('\t'.join([p.name for p in philosophers]))
    print('\t'.join([p.state for p in philosophers]))
    print('\t'.join([str(s.amount).rjust(4, ' ') for s in spaghettis]))
    print()

    print('🍴 Forks state')
    print('=============')
    print('\t'.join([f.used_by or 'Free' for f in forks]))
    print()

# globals
names = ['Frege', 'Plato', 'Quine', 'Rawls', 'Locke']
forks = [Fork() for _ in names]
spaghettis = [Spaghetti() for _ in names]
philosophers = [Philosopher(name, forks[i], forks[(i + 1) % 5]) for i, name in enumerate(names)]
display_cv = Condition()

if __name__ == "__main__":
    main()
